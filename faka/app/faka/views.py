#-*- coding=utf-8 -*-
import datetime
from flask import jsonify, redirect, render_template, request, session, flash, url_for
from flask_login import login_user, logout_user, login_required, current_user
from . import faka
from .. import db
from ..models import *
import requests
from ..payjs import getqr


@faka.route('/')
def index():
    return render_template('faka/index.html')


@faka.route('/getkm', methods=["POST", "GET"])
def getkm():
    if request.method == 'POST':
        lx = request.form.get('tqm', type=int)
        orders = Order.query.filter_by(lx=lx).all()
        if len(orders) == 0:
            return render_template('faka/getkm.html', orders=None)
        else:
            return render_template('faka/getkm.html', orders=orders)
    return render_template('faka/getkm.html')


@faka.route('/get_goods_info', methods=["POST","GET"])
def get_goods_info():
    if request.method == 'POST':
        kms = request.form.get('kms')
        if not kms:
            return render_template('faka/get_goods_info.html')
        kms = kms.split(' ')
        goods_infos = "   "
        for km in kms:
            good = GoodsDetail.query.filter_by(km_value=km).first()
            if good:
                goods_infos += "  --------   "+good.goods_detail + "\n "
        return  render_template('faka/get_goods_info.html', goods_infos=goods_infos)
    return render_template('faka/get_goods_info.html')


@faka.route('/Selgo', methods=["POST"])
def Selgo():
    cateid = request.form.get('cateid')
    goods = db.session.query(Goods).join(
        Category, Goods.cate_id == Category.cateid).filter(Category.cateid == cateid,
                                                           Goods.goods_status==True).all()
    base = u"<option id='{goodid}' imgs='assets/goodsimg/df.jpg' value='{goodid}' kc='{kc}' title='{price}' alt = '{info}'>{goodname}</option>"
    s = u'<option>请选择商品</option>'
    for item in goods:
        kc = db.session.query(KM).join(Goods, KM.goods_id == Goods.goods_id).filter(
            KM.km_status == True, Goods.goods_id == item.goods_id).count()
        s += base.format(goodid=item.goods_id,
                         goodname=item.goods_name, price=item.goods_price, kc=kc, info=item.goods_info)
    return jsonify({'msg': s})


@faka.route('/Checkgo', methods=["POST"])
def Checkgo():
    goodid = request.form.get('goodid')
    kc = db.session.query(KM).join(Goods, KM.goods_id == Goods.goods_id).filter(
        KM.km_status == True, Goods.goods_id == goodid).count()
    if kc < 0:
        retdata = {'code': -1}
    else:
        retdata = {'code': 1}
    return jsonify(retdata)


@faka.route('/CreateOrder', methods=["POST"])
def CreateOrder():
    trade_id = request.form.get('out_trade_no')
    goods_id = request.form.get('gid')
    lx = request.form.get('rel')
    buy_account =  request.form.get('buy_account',type=int)
    feedback = request.scheme + '://' + request.host + '/check_order'

    kc = db.session.query(KM).join(Goods, KM.goods_id == Goods.goods_id).filter(
        KM.km_status == True, Goods.goods_id == goods_id).count()
    if buy_account > kc:
        return {'code': -1}
    try:
        order = Order(trade_id=trade_id, lx=lx, goods_id=goods_id)
        db.session.add(order)
        db.session.commit()
        good = Goods.query.filter_by(goods_id=goods_id).first()
        #qr = getqr(money=good.good_price, tradeid=trade_id,buy_account=buy_account, feedback=feedback)
        qr = True;
        #qr="weixin://wxpay/"
        if qr == False:
            retdata = {'code': -1}
        else:
            retdata = {'code': 0, 'qr': qr}
    except Exception as e:
        print(e)
        retdata = {'code': -1}
    return jsonify(retdata)


@faka.route('/check_order', methods=["POST"])
def check_order():
    tradeid = request.form.get('out_trade_no',type=str)
    return_code = request.form.get('return_code', type=int)
    buy_account = request.form.get('buy_account', type=int)
    if return_code == 1:
        order = Order.query.filter_by(trade_id=tradeid).first()
        print order.trade_status
        if order.trade_status == False:
            # 订单信息更新
            order.endtime = datetime.datetime.now()
            order.trade_status = True
            # 商品信息更新
            goods = Goods.query.filter_by(goods_id=order.goods_id).first()
            goods.goods_sales += buy_account  # 销量加1
            kms = KM.query.filter_by(
                goods_id=goods.goods_id, km_status=True).all()[0:buy_account+1]
            for km in kms:
                km.km_status = False  # 已售出
                km.order_id = order.order_id  # 订单绑定卡密
                db.session.add(km)
            db.session.add(order)
            db.session.add(goods)
            db.session.commit()
    return 'callback success'


# 添加卡密
@faka.route('/admin/add_code/', methods=['POST', 'GET'])
@login_required
def add_code():
    if request.method == 'POST':
        goods_id = request.form.get('goodid')
        kms = eval(request.form.get('km'))
        success, fail = 0, 0
        for km in kms:
            km = km.replace(' ', '')
            try:
                if KM.query.filter_by(km_value=km,goods_id=goods_id).count() == 0:
                    newkm = KM(km_value=km, goods_id=goods_id)
                    db.session.add(newkm)
                    db.session.commit()
                    success += 1
            except Exception as e:
                print(e)
                fail += 1
        msg = '成功{}个，失败{}个'.format(success, fail)
        return jsonify({'msg': msg})
    return render_template('admin/add_code.html')
